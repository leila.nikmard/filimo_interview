import React , {useRef,useEffect} from "react";

import "./style.scss";

const Video = ({data:{attributes}}) => {
    const videoRef = useRef(null);
    const videoContainerRef = useRef(null)

    const handleScroll = () => {
        const videoRefTop = videoContainerRef.current.getBoundingClientRect().top;
        const videoRefOffsetHeight = videoContainerRef.current.offsetHeight;
        const centerWindowHeight = window.innerHeight / 2;
        if( videoRefTop > centerWindowHeight - videoRefOffsetHeight  && videoRefTop < centerWindowHeight){
            videoRef.current.play();
        }else{
            videoRef.current.pause();
        }
    }
    
    useEffect(() => {
        handleScroll();
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    },[]);

    

    return (
        <div ref={videoContainerRef} className="video__box">
            <video ref={videoRef} src={attributes?.preview_src} muted="muted"></video>
        </div>
    )
}

export default Video;