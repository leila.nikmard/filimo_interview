const { createProxyMiddleware } = require('http-proxy-middleware');
import * as config from "./utils/config";

module.exports = function(app) {
  app.use(
    '/video/video/mostViewedVideos',
    createProxyMiddleware({
      target: `${config}`,
      secure: false,
      changeOrigin: true,
    })
  );
};