import React from "react";
import logo from "../../img/logo.png"

import "./style.scss"

const Header = () => {
    return(
        <div className="navbar">
            <div className="navbar__logo">
                <img src={logo}/>
            </div>
        </div>
    )
}

export default Header;