import {ActionTypes} from "../actionTypes";

export const videosList = (data) => {
    return{
        type:ActionTypes.LOAD_ALL_VIDEO,
        data
    }
}