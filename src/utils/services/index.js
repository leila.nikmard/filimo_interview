import axios from "axios"

export const loadAllVideos = () => axios.get(`/video/video/mostViewedVideos`,{
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
});
